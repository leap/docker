FROM registry.0xacab.org/leap/docker/debian:bullseye_amd64

MAINTAINER LEAP Encryption Access Project <info@leap.se>
LABEL Description="Ruby build tools on Debian Bullseye" Vendor="LEAP" Version="3.1"

ENV DEBIAN_FRONTEND noninteractive

# install leap_cli prerequisites, see https://0xacab.org/leap/leap_cli
# and tools needed for ci (moreutils and expect)
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    ruby \
    ruby-dev \
    build-essential \
    rsync \
    openssl \
    rake \
    gcc \
    make \
    zlib1g-dev \
    moreutils \
    tcl \
    expect \
    jq \
    netcat \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN echo '---\n:sources:\n  - https://rubygems.org/\ninstall: --no-document\nupdate: --no-document\n' > /etc/gemrc
RUN gem install bundler
