# Todo:
#   - Use debian-bullseye component for https://download.docker.com/ once it's available
#

FROM debian:bookworm

MAINTAINER LEAP Encryption Access Project <sysdev@leap.se>
LABEL Description="Base debian bookworm baseimage with few customisation" Vendor="LEAP" Version="1.0"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
  && apt-get -y dist-upgrade \
  && apt-get install -y --no-install-recommends \
    git \
    locales \
    ca-certificates \
    curl \
    openssh-client \
    gnupg \
    software-properties-common \
    python3 \
    ca-certificates \
    sudo \
    apt-transport-https \
    golang-go \
    golang-golang-x-text-dev \
    libgtk-3-dev \
    debhelper dh-golang \
    build-essential \
    devscripts \
    pkg-config \
    git-buildpackage \
  && apt-get clean

# Add docker apt keys
RUN install -m 0755 -d /etc/apt/keyrings \
  && curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg \
  && chmod a+r /etc/apt/keyrings/docker.gpg \
# Add the repository to Apt sources:
  && echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null 

RUN apt-get update \
  && apt-get -y dist-upgrade \
  && apt-get -y install docker-ce

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN id cirunner || adduser --group --system --shell /bin/bash cirunner

COPY files/etc /etc/
